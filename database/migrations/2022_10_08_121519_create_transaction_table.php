<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTransactionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transaction', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->unsignedBigInteger('user_id')
                ->index();
            $table->foreign('user_id')
                ->references('id')
                ->on('users');

            $table->decimal('amount', 10, 2);

            $table->text('description')
                ->nullable();

            $table->timestamp('created_at');

            $table->softDeletes();

            $table->index('created_at');
        });

        DB::statement("CREATE EXTENSION pg_trgm;");
        DB::statement("CREATE EXTENSION btree_gin;");
        DB::statement("CREATE INDEX description_gin 
            ON transaction
            USING GIN (description);
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transaction');

        DB::statement("DROP EXTENSION IF EXISTS btree_gin");
        DB::statement("DROP EXTENSION IF EXISTS pg_trgm");
    }
}
