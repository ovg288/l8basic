require('./bootstrap');

import { createApp, h } from 'vue';
import { createInertiaApp } from '@inertiajs/inertia-vue3';
import { InertiaProgress } from '@inertiajs/progress';
import moment from 'moment'

import "bootstrap/dist/css/bootstrap.min.css"
import "bootstrap/dist/js/bootstrap.min.js"
import "bootstrap"

const appName = window.document.getElementsByTagName('title')[0]?.innerText || 'l8basic';

createInertiaApp({
    title: (title) => `${title} - ${appName}`,
    
    resolve: (name) => require(`./Pages/${name}.vue`),

    setup({ el, App, props, plugin }) {
        const app = createApp({ render: () => h(App, props) })
            .use(plugin)
            .mixin({ methods: { route } });

        app.config.globalProperties.$filters = {
            currency(value) {
                return new Intl.NumberFormat('en-US', {
                    minimumFractionDigits: 2,
                }).format(value)
            },
            dateTime(value) {
                if (value) {
                    return moment(String(value)).format('MM/DD hh:mm')
                }

                return 0
            },
            description(str, descriptionLength = 40) {
                if (str != undefined && str.length > descriptionLength) {
                    str = str.substr(0, descriptionLength) + '...'
                }
    
                return str
            }
        }

        app.mount(el)
    },
});

InertiaProgress.init({ color: '#4B5563' });
