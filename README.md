# Description
WIP

# Stack
- Docker
- Vue
- Laravel 8
- Laravel Breeze
- PHP 8
- Postgresql

# Features
- Laravel + vue
- Pagination by index with linear execution time
- Balance change with multiply call protection (where id = and balance =)

# Building
- git pull
- make init
- docker network create external
- docker compose up -d
- make composer install
- make migrate
- make art db:seed

# Using
- `npm run development` - Run development npm watcher
- `make art 'user:create {user_email} --password={password} --balance={balance}'` - User creating command
- `make art 'transaction:proceed --email={user_email} --amount={transaction_amount} --description="{transaction_description}"'` - Transaction with positive amount proceed command
- `make art 'transaction:proceed --email={user_email} --amount=-{transaction_amount} --description="{transaction_description}"'` - Transaction with negative amount proceed command

# Backlog
T100: DB Initialization (db creation) \
T101: Request validation \
T102: Response validation \
T103: Templates cleanup \
T104: DOM elements classes cleanup \
T105: Using gin index for search by Description \
~T106: phpcbf, phpstan~ \
T107: Phpcbf fixes \
T108: Phpstan level2 fixes \
T109: Phpstan level4 fixes \
T110: Phpstan level6 fixes 