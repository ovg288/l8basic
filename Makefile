SERVICE_NAME='l8basic'

ifeq (composer, $(firstword $(MAKECMDGOALS)))
  RUN_ARGS := $(wordlist 2,$(words $(MAKECMDGOALS)),$(MAKECMDGOALS))
  $(eval $(RUN_ARGS):;@:)
endif

ifeq (art, $(firstword $(MAKECMDGOALS)))
  RUN_ARGS := $(wordlist 2,$(words $(MAKECMDGOALS)),$(MAKECMDGOALS))
  $(eval $(RUN_ARGS))
endif

ifeq (exec, $(firstword $(MAKECMDGOALS)))
  RUN_ARGS := $(wordlist 2,$(words $(MAKECMDGOALS)),$(MAKECMDGOALS))
  $(eval $(RUN_ARGS):;@:)
endif

# Composer runner:
# - make composer about
# - make composer install
#
composer:
	docker exec ${SERVICE_NAME}_php_fpm composer $(RUN_ARGS)

exec:
	docker exec ${SERVICE_NAME}_php_fpm $(RUN_ARGS)

phpstan:
	docker exec ${SERVICE_NAME}_php_fpm vendor/bin/phpstan analyse app --memory-limit 1G --level 3

# Console runner
# - make artisan make:command CodeGen
art:
	docker exec $(SERVICE_NAME)_php_fpm php artisan $(RUN_ARGS)

# This is default Target.
# Also will be use on command `make` without arguments
#
setup: build db-prepare

setup-dev: build-dev db-prepare

init:
	cp -n .env.example .env

# Do build and up containers with use docker-compose
#
build:
	docker compose -f docker-compose.yml up -d

# Do Rebuild and up containers with use docker-compose
#
rebuild:
	docker compose up -d --build --force-recreate

# Cleanup service containers
#
cluster-clean:
	docker rm -f ${SERVICE_NAME}_webserver
	docker rm -f ${SERVICE_NAME}_php_fpm
	docker rm -f ${SERVICE_NAME}_rabbitmq
	docker rm -f ${SERVICE_NAME}_clickhouse
	docker rm -f ${SERVICE_NAME}_postgres

migrate:
	docker exec ${SERVICE_NAME}_php_fpm php artisan migrate
