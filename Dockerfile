FROM node:16-alpine

WORKDIR /usr/src/app

COPY package*.json ./
COPY tailwind*.js ./
COPY webpack*.js ./

RUN npm install -g npm
RUN npm install --legacy-peer-deps

COPY . .

RUN chown -R 1000:1000 /root/.npm
RUN chmod a+w -R /root/.npm

CMD ["npm", "run", "watch"]