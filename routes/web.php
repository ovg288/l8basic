<?php

use App\Http\Controllers\TransactionController;
use App\Http\Controllers\UserController;
use Illuminate\Foundation\Application;
use Illuminate\Support\Facades\Route;
use Inertia\Inertia;

Route::get('/', function () {
    return Inertia::render('Welcome', [
        'canLogin' => Route::has('login'),
        'canRegister' => Route::has('register'),
        'laravelVersion' => Application::VERSION,
        'phpVersion' => PHP_VERSION,
    ]);
});

Route::get('/dashboard', function () {
    return Inertia::render('Dashboard');
})->middleware(['auth', 'verified'])->name('dashboard');

Route::get('/transactions', function () {
    return Inertia::render('Transactions');
})->middleware(['auth', 'verified'])->name('transactions');

Route::middleware('auth')->group(function () {
    Route::get('/user/info', [UserController::class, 'info']);
    Route::get('/transactions/list/{count?}/{transactionId?}', [TransactionController::class, 'list'])
        ->where('count', '[0-9]+')
        ->where('transactionId', '[0-9]+');

    Route::post('/transactions/list', [TransactionController::class, 'filteredList'])
        ->where('count', '[0-9]+')
        ->where('transactionId', '[0-9]+');
});

Route::get('/info', [UserController::class, 'info'])
    ->middleware('auth')
    ->name('user_info');

require __DIR__.'/auth.php';
