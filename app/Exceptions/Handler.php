<?php

namespace App\Exceptions;

use App\Exceptions\Balance\BalanceChangedDuringTransactionException;
use App\Exceptions\Balance\InsufficientBalanceException;
use App\Exceptions\User\CreateUserQueryException;
use App\Exceptions\User\CreateUserUnknownException;
use App\Exceptions\User\UserAlreadyExistsException;
use App\Exceptions\User\UserNotAuthenticatedException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Register the exception handling callbacks for the application.
     *
     * @return void
     */
    public function register()
    {
        $this->reportable(function (CreateUserQueryException $e) {
        })->stop();
        $this->reportable(function (CreateUserUnknownException $e) {
        })->stop();
        $this->reportable(function (UserAlreadyExistsException $e) {
        })->stop();
        $this->reportable(function (UserNotAuthenticatedException $e) {
        })->stop();

        $this->reportable(function (InsufficientBalanceException $e) {
        })->stop();
        $this->reportable(function (BalanceChangedDuringTransactionException $e) {
        })->stop();
    }
}
