<?php

namespace App\Exceptions\User;

use Exception;

class CreateUserUnknownException extends Exception
{
    public function __construct(
        protected $message,
        protected $code = 1000,
    ) {
    }
}
