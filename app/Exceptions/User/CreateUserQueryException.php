<?php

namespace App\Exceptions\User;

use Exception;

class CreateUserQueryException extends Exception
{
    public function __construct(
        protected $message = 'Create User query exception',
        protected $code = 1001,
    ) {
    }
}
