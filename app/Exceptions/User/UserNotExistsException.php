<?php

namespace App\Exceptions\User;

use Exception;

class UserNotExistsException extends Exception
{
    public function __construct(
        protected $message = 'User does not exists',
        protected $code = 1004,
    ) {
    }
}
