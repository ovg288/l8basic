<?php

namespace App\Exceptions\User;

use Exception;

class UserNotAuthenticatedException extends Exception
{
    public function __construct(
        protected $message = 'User is not authenticated',
        protected $code = 1003,
    ) {
    }
}
