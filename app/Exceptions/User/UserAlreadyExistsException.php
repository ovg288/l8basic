<?php

namespace App\Exceptions\User;

use Exception;

class UserAlreadyExistsException extends Exception
{
    public function __construct(
        protected $message = 'User already exists',
        protected $code = 1002,
    ) {
    }
}
