<?php

namespace App\Exceptions\Transaction;

use Exception;

class UnknownSortOrderException extends Exception
{
    public function __construct(
        protected $message = 'Unknown sort order!',
        protected $code = 2001,
    ) {
    }
}
