<?php

namespace App\Exceptions\User;

use Exception;

class UnknownTransactionProceedException extends Exception
{
    public function __construct(
        protected $message,
        protected $code = 2002,
    ) {
    }
}
