<?php

namespace App\Exceptions\Transaction;

use Exception;

class ZeroAmountTransactionException extends Exception
{
    public function __construct(
        protected $message = 'Transaction with zero amount!',
        protected $code = 2000,
    ) {
    }
}
