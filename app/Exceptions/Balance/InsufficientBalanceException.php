<?php

namespace App\Exceptions\Balance;

use Exception;

class InsufficientBalanceException extends Exception
{
    public function __construct(
        protected $message = 'Balance is less than the transaction amount',
        protected $code = 3000,
    ) {
    }
}
