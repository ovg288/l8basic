<?php

namespace App\Exceptions\Balance;

use Exception;

class BalanceChangedDuringTransactionException extends Exception
{
    public function __construct(
        protected $message = 'Balance has changed during the transaction!',
        protected $code = 3001,
    ) {
    }
}
