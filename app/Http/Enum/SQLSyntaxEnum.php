<?php

namespace App\Http\Enum;

class SQLSyntaxEnum
{
    public const ORDER_ASCENDING = 'ASC';
    public const ORDER_DESCENDING = 'DESC';
}
