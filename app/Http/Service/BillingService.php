<?php

namespace App\Http\Service;

use App\Exceptions\Balance\InsufficientBalanceException;
use App\Exceptions\Transaction\ZeroAmountTransactionException;
use App\Exceptions\User\UnknownTransactionProceedException;
use Exception;
use App\Http\Service\TransactionService;
use App\Models\Transaction;
use App\Models\User;
use Illuminate\Support\Facades\DB;

class BillingService
{
    public function __construct(
        protected TransactionService $transactionService,
        protected UserService $userService,
    ) {
    }

    /**
     * The functionality of the transaction in two stages:
     * 1. Create a Transaction record;
     * 2. Apply a Transaction amount to User Balance;
     *
     * - Transaction amount must not be 0;
     * - In case of a negative Transaction amount, the User's balance must be
     * grater than the Transaction amount
     *
     * Checks:
     * - Transaction amount;
     * - User Balance sufficiency
     *
     * @param User $user
     * @param float $amount
     * @param string|null $description
     *
     * @return Transaction
     *
     * @throws ZeroAmountTransactionException
     * @throws UnknownTransactionProceedException
     */
    public function transactionProceed(
        User $user,
        float $amount,
        ?string $description = null
    ): Transaction {
        DB::beginTransaction();

        $amount = round($amount, 2, PHP_ROUND_HALF_UP);

        // Validate Transaction amount
        if (abs($amount) === 0.00) {
            throw new ZeroAmountTransactionException();
        }

        // Validate User balance
        $userBalance = $user->balance;
        if ($amount < 0 && $userBalance < abs($amount)) {
            DB::rollBack();
            throw new InsufficientBalanceException();
        }

        // Create Transaction
        try {
            $transaction = $this->transactionService->create(
                $user,
                $amount,
                $description,
            );
        } catch (Exception $e) {
            DB::rollBack();
            throw new UnknownTransactionProceedException($e->getMessage());
        }

        // Apply to User balance
        try {
            $this->userService->applyTransaction($user, $userBalance, $transaction);
        } catch (Exception $e) {
            DB::rollBack();
            throw new UnknownTransactionProceedException($e->getMessage());
        }

        DB::commit();

        return $transaction->fresh();
    }
}
