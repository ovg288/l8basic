<?php

namespace App\Http\Service;

use App\Exceptions\Transaction\UnknownSortOrderException;
use App\Models\Transaction;
use App\Models\User;
use App\Http\Repository\TransactionRepository;
use Illuminate\Support\Collection;
use App\Http\Enum\SQLSyntaxEnum;

class TransactionService
{
    public function __construct(
        private TransactionRepository $transactionRepository,
    )
    {}

    /**
     * Returns User transaction list, filtered by passed searchString and 
     * sorted by date. Result limit is implemented using a passed parameters
     * of a limit and an identifier of the last loaded transaction.
     *
     * @param User $user
     * @param integer $limit
     * @param string $dateSortOrder
     * @param string $searchString
     * 
     * @return Collection
     */
    public function list(
        User $user, 
        int $limit, 
        int $lastLoadedId,
        string $dateSortOrder,
        ?string $searchString,
    ): Collection
    {
        if (!in_array(mb_strtoupper($dateSortOrder), [
            SQLSyntaxEnum::ORDER_ASCENDING,
            SQLSyntaxEnum::ORDER_DESCENDING
        ])) {
            throw new UnknownSortOrderException();
        }

        $criteria = [['user_id', '=', $user->id]];

        if (!empty($searchString)) {
            $criteria[] = [
                'description', 'ilike', '%' . $searchString . '%'
            ];
        }

        if ($lastLoadedId > 0 && $dateSortOrder == SQLSyntaxEnum::ORDER_DESCENDING) {
            $criteria[] = ['id', '<', $lastLoadedId];
        } elseif ($lastLoadedId > 0 && $dateSortOrder == SQLSyntaxEnum::ORDER_ASCENDING) {
            $criteria[] = ['id', '>', $lastLoadedId];
        }

        $transactionList = $this->transactionRepository->read(
            $criteria,
            $limit,
            'created_at',
            $dateSortOrder,
            $searchString
        );

        return $transactionList;
    }

    /**
     * Creates a Transaction instance
     *
     * @param User $user
     * @param float $amount
     * @param string|null $description
     * 
     * @return Transaction
     */
    public function create(
        User $user, 
        float $amount, 
        ?string $description = null
    ): Transaction
    {
        return $this->transactionRepository->create(
            $user, 
            $amount, 
            $description
        );
    }
}
