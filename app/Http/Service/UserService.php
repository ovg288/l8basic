<?php

namespace App\Http\Service;

use App\Models\Transaction;
use App\Models\User;
use App\Http\Repository\UserRepository;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

use Illuminate\Database\QueryException;
use App\Exceptions\User\CreateUserQueryException;
use App\Exceptions\User\CreateUserUnknownException;
use App\Exceptions\User\UserAlreadyExistsException;
use App\Exceptions\User\UserNotAuthenticatedException;
use App\Exceptions\Balance\BalanceChangedDuringTransactionException;

use Exception;

class UserService
{
    public const DEFAULT_BALANCE = 1000;

    public function __construct(
        private UserRepository $userRepository,
    )
    {}

    /**
     * Returns user by passed email
     * 
     * @property string $email
     * 
     * @return null|User $user
     */
    public function getByEmail(string $email): ?User
    {
        $user = $this->userRepository->getByEmail($email);

        return $user;
    }

    /**
     * Creates new User with the given arguments and returns the generated User
     * model
     *
     * @param string $name
     * @param string $email
     * @param float $balance
     * 
     * @return User
     * 
     * @throws UserAlreadyExistsException
     * @throws CreateUserQueryException
     * @throws CreateUserUnknownException
     */
    public function createUser(
        string $name,
        string $email,
        string $password,
        ?float $balance,
    ): User
    {
        $password = Hash::make($password);

        $balance = $balance ?? static::DEFAULT_BALANCE;

        $user = $this->getByEmail($email);

        if ($user !== null) {
            throw new UserAlreadyExistsException();
        }

        try {
            $user = $this->userRepository->create($name, $email, $password, $balance);
        } catch(QueryException $e) {
            throw new CreateUserQueryException($e->getMessage());
        } catch(Exception $e) {
            throw new CreateUserUnknownException($e->getMessage());
        }

        return $user;
    }

    /**
     * Returns authenticated User model
     *
     * @return User
     */
    public function authenticatedUser(): User
    {
        $user = Auth::user();

        if ($user === null) {
            throw new UserNotAuthenticatedException();
        }

        return $user;
    }

    /**
     * Applies transaction amount to the User's balance
     *
     * @param User $user
     * @param float $initialBalance
     * @param Transaction $transaction
     * 
     * @return User
     */
    public function applyTransaction(
        User $user, 
        float $initialBalance,
        Transaction $transaction
    ): User
    {
        $newBalance = $user->balance + $transaction->amount;

        $affected = $this->userRepository->setBalance($user, $initialBalance, $newBalance);
        
        if ($affected === 0) {
            throw new BalanceChangedDuringTransactionException();
        }

        return $user;
    }
}