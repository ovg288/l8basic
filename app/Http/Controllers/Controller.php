<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\JsonResponse;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests;
    use DispatchesJobs;
    use ValidatesRequests;

    /**
     * Success response method
     *
     * @param mixed $responseData
     * @return JsonResponse
     */
    protected function successResponse(mixed $responseData): JsonResponse
    {
        return $this->response($responseData, 200, [], []);
    }

    /**
     * Failed response method
     *
     * @param mixed $responseData
     * @return JsonResponse
     */
    protected function failResponse(mixed $responseData): JsonResponse
    {
        return $this->response($responseData, 500, [], []);
    }

    /**
     * Base response method
     *
     * @param mixed $data
     * @param integer $status
     * @param array $headers
     * @param array $options
     *
     * @return JsonResponse
     */
    protected function response(
        mixed $data,
        int $status = 200,
        array $headers = [],
        array $options = [],
    ): JsonResponse {
        $dataJson = json_encode($data);

        return new JsonResponse($dataJson, $status, $headers, $options, true);
    }
}
