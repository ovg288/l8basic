<?php

namespace App\Http\Controllers;

use Illuminate\Http\JsonResponse;
use App\Http\Service\UserService;

class UserController extends Controller
{
    public function __construct(
        private UserService $userService
    ) {
    }

    /**
     * Returns authenticated user data
     *
     * @return JsonResponse
     */
    public function info(): JsonResponse
    {
        return $this->successResponse(
            $this->userService->authenticatedUser()
        );
    }
}
