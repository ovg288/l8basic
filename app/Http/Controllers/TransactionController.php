<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreTransactionRequest;
use App\Http\Requests\UpdateTransactionRequest;
use App\Models\Transaction;
use App\Http\Service\TransactionService;
use App\Http\Service\UserService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class TransactionController extends Controller
{
    /**
     * @param TransactionService $transactionService
     * @param UserService $userService
     */
    public function __construct(
        private TransactionService $transactionService,
        private UserService $userService,
    )
    {}

    public function filteredList(Request $request): JsonResponse
    {
        $count = $request->get('count', 5);
        $lastLoadedTransactionId = $request->get('transactionId', 0);
        $dateSortOrder = $request->get('dateOrder', 'DESC');
        $searchString = $request->get('searchString', '');

        return $this->successResponse($this->transactionService->list(
            $this->userService->authenticatedUser(), 
            $count,
            $lastLoadedTransactionId,
            $dateSortOrder,
            $searchString,
        ));
    }
    
    /**
     * Display a listing of the resource.
     * 
     * @param int $count
     * @param int $lastLoadedTransactionId
     *
     * @return JsonResponse
     */
    public function list(Request $request, int $count = 5, int $lastLoadedTransactionId = 0): JsonResponse
    {
        dd($this->transactionService->list(
            $this->userService->authenticatedUser(), 
            $count,
            $lastLoadedTransactionId,
            'DESC',
            ''
        ));

        return $this->successResponse($this->transactionService->list(
            $this->userService->authenticatedUser(), 
            $count,
            $lastLoadedTransactionId,
            'DESC',
            ''
        ));
    }
}
