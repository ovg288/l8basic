<?php

namespace App\Http\Repository;

use App\Models\User;

class UserRepository
{
    /**
     * Creates a new User record
     *
     * @param string $name
     * @param string $email
     * @param string $password
     * @param float $balance
     *
     * @return User
     */
    public function create(
        string $name,
        string $email,
        string $password,
        float $balance,
    ): User {
        $user = User::create([
            'name' => $name,
            'email' => $email,
            'password' => $password,
            'balance' => $balance
        ]);

        return $user;
    }

    /**
     * Returns User instance by email
     *
     * @param string $email
     * @return User|null
     */
    public function getByEmail(string $email): ?User
    {
        return User::where('email', $email)
            ->first();
    }

    /**
     * Sets the User balance
     *
     * @param User $user
     * @param float $initialBalance
     * @param float $newBalance
     *
     * @return boolean
     */
    public function setBalance(
        User $user,
        float $initialBalance,
        float $newBalance,
    ): bool {
        $affected = User::where('id', $user->id)
            ->where('balance', $initialBalance)
            ->update(['balance' => $newBalance]);

        return $affected > 0;
    }
}
