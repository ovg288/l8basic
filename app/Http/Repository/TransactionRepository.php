<?php

namespace App\Http\Repository;

use App\Models\Transaction;
use App\Models\User;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Collection;

class TransactionRepository
{
    use SoftDeletes;

    protected const TABLE = 'transaction';

    /**
     * Undocumented function
     *
     * @param User $user
     * @param float $amount
     * @param string|null $description
     *
     * @return Transaction
     */
    public function create(
        User $user,
        float $amount,
        ?string $description = null
    ): Transaction {
        $transaction = Transaction::create([
            'user_id' => $user->id,
            'amount' => $amount,
            'description' => $description,
        ]);

        $transaction->user()->associate($user);

        $transaction->save();


        return $transaction;
    }

    /**
     * Undocumented function
     *
     * @param array $criteria
     * @param integer|null $limit
     * @param string $sortOrder
     * @param string $sortDirection
     *
     * @return Collection
     */
    public function read(
        array $criteria,
        int $limit = null,
        string $sortOrder = 'created_at',
        string $sortDirection = 'desc',
    ): Collection {
        $queryBuilder = Transaction::where($criteria)
            ->orderBy($sortOrder, $sortDirection)
            ->orderBy('id', $sortDirection);

        if ($limit !== null) {
            $queryBuilder->limit($limit);
        }

        return $queryBuilder->get();
    }
}
