<?php

namespace App\Console\Commands;

use App\Exceptions\User\UserNotExistsException;
use App\Http\Service\BillingService;
use App\Http\Service\UserService;
use Illuminate\Console\Command;
use Exception;

class ProceedTransaction extends Command
{
    private const ERROR_USER_IS_NOT_EXISTS = 'User does not exists!';

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'transaction:proceed {--email=} {--amount=} {--description=}
        {--email: User email}
        {--amount: Transaction amount}
        {--description: Transaction description. Default: null}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command creates a transaction for User passed by
        email. Negative User balance is prohibited';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(
        private BillingService $billingService,
        private UserService $userService,
    ) {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $this->info('Transaction maker');

        $userEmail = $this->option('email');
        $amount = (float)$this->option('amount');
        $description = $this->option('description');

        $user = $this->userService->getByEmail($userEmail);

        if ($user === null) {
            $this->error(static::ERROR_USER_IS_NOT_EXISTS);
            throw new UserNotExistsException();
            return Command::INVALID;
        }

        try {
            $transaction = $this->billingService->transactionProceed(
                $user,
                $amount,
                $description,
            );
        } catch (Exception $ex) {
            $this->error($ex->getMessage());
            return Command::FAILURE;
        }

        $this->info(sprintf(
            'Transaction [%d] %.2f was made. User balance: %.2f',
            $transaction->id,
            $transaction->amount,
            $transaction->user->balance,
        ));

        return Command::SUCCESS;
    }
}
