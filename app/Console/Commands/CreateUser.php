<?php

namespace App\Console\Commands;

use App\Http\Service\UserService;
use Illuminate\Console\Command;
use Illuminate\Database\QueryException;
use App\Exceptions\User\CreateUserQueryException;
use App\Exceptions\User\CreateUserUnknownException;
use App\Exceptions\User\UserAlreadyExistsException;

class CreateUser extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'user:create {email} {--name=} {--password=password} {--balance=0}
                            {--name: User name}
                            {--password: User password. Default: `password`}
                            {--balance: Account balance. Default: 0}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'The command creates a user based on passed email. 
        Available options are name, password and balance.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(private UserService $userService)
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $this->info('User creator');

        $email = $this->argument('email');

        $name = !$this->option('name') ? stristr($email, '@', true) : $this->option('name');
        $password = $this->option('password');
        $balance = $this->option('balance');

        try {
            $user = $this->userService->createUser(
                $name,
                $email,
                $password,
                $balance
            );
        } catch (
            UserAlreadyExistsException |
            CreateUserQueryException |
            CreateUserUnknownException |
            QueryException $e
        ) {
            $this->error($e->getMessage());

            return Command::FAILURE;
        }

        $this->info(
            sprintf('User [%d] %s created!', $user->id, $user->email)
        );

        return Command::SUCCESS;
    }
}
